<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->integer('user_id');
            $table->integer('category_id');
            $table->integer('author_id');
            $table->integer('publisher_id');
            $table->integer('min_qty');

            $table->string('pages');
            $table->string('dimension');
            $table->date('publication_date');
            $table->string('illustration_note');
            $table->string('isbn10')->nullable();
            $table->string('isbn13')->nullable();
            $table->string('image');

            $table->double('unit_price');
            $table->double('discount_price')->nullable();
            $table->integer('discount_type')->nullable();
            $table->string('discount')->nullable();
            $table->text('details')->nullable();
            $table->string('book_format');
            $table->string('language');
            $table->string('variant');
            $table->string('slug');
            $table->integer('num_of_sale')->defined(0);
            $table->boolean('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
