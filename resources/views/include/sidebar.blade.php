<div class="app-sidebar colored">
    <div class="sidebar-header">
        <a class="header-brand" href="{{route('dashboard')}}">
            <div class="logo-img">
               <img height="30" src="{{ asset('img/logo_white.png')}}" class="header-brand-img" title="RADMIN">
            </div>
        </a>
        <div class="sidebar-action"><i class="ik ik-arrow-left-circle"></i></div>
        <button id="sidebarClose" class="nav-close"><i class="ik ik-x"></i></button>
    </div>

    @php
        $segment1 = request()->segment(1);
        $segment2 = request()->segment(2);
        $segment3 = request()->segment(3);
    @endphp
    
    <div class="sidebar-content">
        <div class="nav-container">
            <nav id="main-menu-navigation" class="navigation-main">
                <div class="nav-item {{ ($segment1 == 'dashboard') ? 'active' : '' }}">
                    <a href="{{route('dashboard')}}"><i class="ik ik-bar-chart-2"></i><span>{{ __('Dashboard')}}</span></a>
                </div>
                <div class="nav-item {{ ($segment2 == 'users' || $segment2 == 'roles'||$segment2 == 'permission' ||$segment2 == 'user') ? 'active open' : '' }} has-sub">
                    <a href="#"><i class="ik ik-user"></i><span>{{ __('Adminstrator')}}</span></a>
                    <div class="submenu-content">
                        <!-- only those have manage_user permission will get access -->
                        @can('manage_user')
                        <a href="{{url('users')}}" class="menu-item {{ ($segment2 == 'users') ? 'active' : '' }}">{{ __('Users')}}</a>
                        <a href="{{url('user/create')}}" class="menu-item {{ ($segment2 == 'user' && $segment3 == 'create') ? 'active' : '' }}">{{ __('Add User')}}</a>
                         @endcan
                         <!-- only those have manage_role permission will get access -->
                        @can('manage_roles')
                        <a href="{{url('roles')}}" class="menu-item {{ ($segment2 == 'roles') ? 'active' : '' }}">{{ __('Roles')}}</a>
                        @endcan
                        <!-- only those have manage_permission permission will get access -->
                        @can('manage_permission')
                        <a href="{{url('permission')}}" class="menu-item {{ ($segment2 == 'permission') ? 'active' : '' }}">{{ __('Permission')}}</a>
                        @endcan
                    </div>
                </div>

                <div class="nav-item {{ ($segment2 == 'authors') ? 'active open' : '' }} has-sub">
                    <a href="#"><i class="ik ik-user"></i><span>{{ __('Author')}}</span></a>
                    <div class="submenu-content">
                        <!-- only those have manage_author permission will get access -->
                        @can('manage_user')
                        <a href="{{ route('authors.index') }}" class="menu-item {{ ($segment2 == 'authors' && $segment3 == null) ? 'active' : '' }}">{{ __('Authors')}}</a>
                        <a href="{{ route('authors.create') }}" class="menu-item {{ ($segment2 == 'authors' && $segment3 == 'create') ? 'active' : '' }}">{{ __('Add Author')}}</a>
                         @endcan
                    </div>
                </div>

                <div class="nav-item {{ ($segment2 == 'publisher') ? 'active open' : '' }} has-sub">
                    <a href="#"><i class="ik ik-user"></i><span>{{ __('Publisher')}}</span></a>
                    <div class="submenu-content">
                        <!-- only those have manage_author permission will get access -->
                        @can('manage_user')
                        <a href="{{ route('publisher.index') }}" class="menu-item {{ ($segment2 == 'publisher' && $segment3 == null) ? 'active' : '' }}">{{ __('Publishers')}}</a>
                        <a href="{{ route('publisher.create') }}" class="menu-item {{ ($segment2 == 'publisher' && $segment3 == 'create') ? 'active' : '' }}">{{ __('Add Publishers')}}</a>
                         @endcan
                    </div>
                </div>
                <div class="nav-item {{ ($segment2 == 'categories') ? 'active open' : '' }} has-sub">
                    <a href="#"><i class="ik ik-user"></i><span>{{ __('Category')}}</span></a>
                    <div class="submenu-content">
                        <!-- only those have manage_author permission will get access -->
                        @can('manage_category')
                        <a href="{{ route('categories.index') }}" class="menu-item {{ ($segment2 == 'categories' && $segment3 == null) ? 'active' : '' }}">{{ __('Categories')}}</a>
                        <a href="{{ route('categories.create') }}" class="menu-item {{ ($segment2 == 'categories' && $segment3 == 'create') ? 'active' : '' }}">{{ __('Add Category')}}</a>
                         @endcan
                    </div>
                </div>

                <div class="nav-item {{ ($segment2 == 'products') ? 'active open' : '' }} has-sub">
                    <a href="#"><i class="ik ik-user"></i><span>{{ __('Product')}}</span></a>
                    <div class="submenu-content">
                        <!-- only those have manage_author permission will get access -->
                        @can('manage_category')
                        <a href="{{ route('products.index') }}" class="menu-item {{ ($segment2 == 'products' && $segment3 == null) ? 'active' : '' }}">{{ __('Products')}}</a>
                        <a href="{{ route('products.create') }}" class="menu-item {{ ($segment2 == 'products' && $segment3 == 'create') ? 'active' : '' }}">{{ __('Add Product')}}</a>
                        <a href="{{ route('products.stock') }}" class="menu-item {{ ($segment2 == 'products' && $segment3 == 'stock') ? 'active' : '' }}">{{ __('Product Stock')}}</a>
                        @endcan
                    </div>
                </div>

                <div class="nav-item has-sub">
                    <a href="#"><i class="ik ik-user"></i><span>{{ __('Items')}}</span></a>
                    <div class="submenu-content">
                        <!-- only those have manage_author permission will get access -->
                        <a href="{{ route('book_formats.index') }}" class="menu-item">{{ __('Book Format')}}</a>
                        <a href="{{ route('languages.index') }}" class="menu-item">{{ __('Book Language')}}</a>
                    </div>
                </div>

                <div class="nav-item has-sub">
                    <a href="#"><i class="ik ik-user"></i><span>{{ __('Testimonials')}}</span></a>
                    <div class="submenu-content">
                        <!-- only those have manage_author permission will get access -->
                        <a href="{{ route('testimonials.index') }}" class="menu-item">{{ __('Testimonial')}}</a>
                        <a href="{{ route('testimonials.create') }}" class="menu-item">{{ __('Add Testimonial')}}</a>
                    </div>
                </div>

            </nav>
        </div>
    </div>
</div>
