
<script src="{{ asset('vendor/sweetalert/sweetalert.all.js') }}"></script>
@if (session('success'))
  <script>
      Swal.fire(
        "Good job!",
        "{!! session('success') !!}",
        "success"
    )
  </script>
@endif
@if (session('error'))
  <script>
        Swal.fire(
        icon: 'error',
        title: 'Oops...',
        text: "{!! session('error') !!}",
        );
  </script>
@endif