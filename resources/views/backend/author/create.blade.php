@extends('layouts.main')
@section('title', 'Authors')
@section('content')
    
<div class="container-fluid">
<div class="page-header">
    <div class="row align-items-end">
        <div class="col-lg-8">
            <div class="page-header-title">
                <i class="ik ik-user-plus bg-blue"></i>
                <div class="d-inline">
                    <h5>{{ __('Add Authors')}}</h5>
                    <span>{{ __('Create new authors')}}</span>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <nav class="breadcrumb-container" aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="{{url('dashboard')}}"><i class="ik ik-home"></i></a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="#">{{ __('Add Authors')}}</a>
                    </li>
                </ol>
            </nav>
        </div>
    </div>
</div>
<div class="row">
    <!-- start message area-->
    @include('include.message')
    <!-- end message area-->
    <div class="col-md-12">
        <div class="card ">
            <div class="card-header">
                <h3>{{ __('Add Authors')}}</h3>
            </div>
            <div class="card-body">
                {{ Form::open(array('route'=>['authors.store'],'method'=>'POST','files'=>true)) }}
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="name">{{ __('Author Name')}}<span class="text-red">*</span></label>
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="" placeholder="Enter auther name" required>
                                <div class="help-block with-errors"></div>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="details">{{ __('Authers Details')}}<span class="text-red">*</span></label>
                                <textarea class="summernote" name="details"></textarea>
                                <div class="help-block with-errors"></div>

                                @error('details')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label>{{ __('Upload Image') }}<span class="text-red">*</span></label>
                                <input type="file" name="image" class="file-upload-default" onchange="loadFile(event)">
                                <div class="input-group col-xs-12">
                                    <input type="text" name="photo" class="form-control file-upload-info" disabled placeholder="Upload Image">
                                    <span class="input-group-append">
                                    <button class="file-upload-browse btn btn-primary" type="button">Upload</button>
                                    </span>
                                </div>
                                <img id="output" width="20%" style="margin-top:5px;" />

                                <div class="help-block with-errors"></div>
                                @error('image')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>


                        <div class="col-md-12">
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">{{ __('Submit')}}</button>
                            </div>
                        </div>
                    </div>

                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
</div>
<!-- push external js -->
@endsection
