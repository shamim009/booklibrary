@extends('layouts.main')
@section('title', 'Author | List')
@section('content')
    
    <div class="container-fluid">
    	<div class="page-header">
            <div class="row align-items-end">
                <div class="col-lg-8">
                    <div class="page-header-title">
                        <i class="ik ik-users bg-blue"></i>
                        <div class="d-inline">
                            <h5>{{ __('Author')}}</h5>
                            <span>{{ __('List of Author')}}</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <nav class="breadcrumb-container" aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="{{route('dashboard')}}"><i class="ik ik-home"></i></a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="#">{{ __('Author')}}</a>
                            </li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
        <div class="row">
            <!-- start message area-->
            @include('include.message')
            <!-- end message area-->
            <div class="col-md-12">
                <div class="card p-3">
                    <div class="card-header"><h3>{{ __('Author')}}</h3></div>
                    <div class="card-body">
                        <table id="data_table" class="table">
                            <thead>
                                <tr>
                                    <th>{{ __('Sl')}}</th>
                                    <th>{{ __('Name')}}</th>
                                    <th>{{ __('Image') }}</th>
                                    <th>{{ __('Description') }}</th>
                                    <th>{{ __('Status') }}</th>
                                    <th style="text-align:right;">{{ __('Action')}}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($author_all as $key=>$item)
                                     <tr>
                                         <td>{{ $key+1 }}</td>
                                         <td>{{ $item->name }}</td>
                                         <td>
                                            <img src="{{ asset('/backend/author/'.$item->image) }}" class="table-user-thumb" alt="">
                                         </td>
                                         <td>{!! $item->details !!}</td>
                                         <td>
                                             @if ($item->status == 1)
                                               <span class="badge badge-success m-1">Active</span>
                                             @else
                                               <span class="badge badge-danger m-1">Inactive</span>
                                             @endif
                                         </td>
                                         <td>
                                            <div class="table-actions">
                                                <a href="{{ route('authors.edit',$item->id) }}"><i class="ik ik-edit-2 text-green"></i></a>
                                                <a href="{{ route('authors.delete',$item->id) }}" onclick="return confirmDelete()"><i class="ik ik-trash-2 text-red"></i></a>
                                            </div>
                                         </td>
                                     </tr> 
                                @empty
                                    No data found
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
    <!-- push external js -->
@endsection
