@extends('layouts.main')
@section('title', 'Testimonials')
@section('content')
    
<div class="container-fluid">
<div class="page-header">
    <div class="row align-items-end">
        <div class="col-lg-8">
            <div class="page-header-title">
                <i class="ik ik-user-plus bg-blue"></i>
                <div class="d-inline">
                    <h5>{{ __('Add Testimonials')}}</h5>
                    <span>{{ __('Create new testimonials')}}</span>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <nav class="breadcrumb-container" aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="{{url('dashboard')}}"><i class="ik ik-home"></i></a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="#">{{ __('Add Testimonials')}}</a>
                    </li>
                </ol>
            </nav>
        </div>
    </div>
</div>
<div class="row">
    <!-- start message area-->
    @include('include.message')
    <!-- end message area-->
    <div class="col-md-12">
        <div class="card ">
            <div class="card-header">
                <h3>{{ __('Add Testimonials')}}</h3>
            </div>
            <div class="card-body">
                {{ Form::open(array('route'=>['testimonials.store'],'method'=>'POST','files'=>true)) }}
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="name">{{ __('Name')}}<span class="text-red">*</span></label>
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" placeholder="Enter name" required>
                                <div class="help-block with-errors"></div>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>    
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="name">{{ __('Company Name')}}<span class="text-red">*</span></label>
                                <input id="name" type="text" class="form-control @error('company_name') is-invalid @enderror" name="company_name" placeholder="Enter company name" required>
                                <div class="help-block with-errors"></div>

                                @error('company_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>  
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="details">{{ __('Designation')}}<span class="text-red">*</span></label>
                                <input id="name" type="text" class="form-control @error('designation') is-invalid @enderror" name="designation" placeholder="Enter designation" required>
                                <div class="help-block with-errors"></div>

                                @error('designation')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-sm-12"> 
                            <div class="form-group">
                                <label for="details">{{ __('Short Review')}}<span class="text-red">*</span></label>
                                <textarea class="summernote" name="review"></textarea>
                                <div class="help-block with-errors"></div>

                                @error('review')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>    
                        <div class="col-sm-12"> 
                            <div class="form-group">
                                <label>{{ __('Upload Image') }}<span class="text-red">*</span></label>
                                <input type="file" name="image" class="file-upload-default" onchange="loadFile(event)">
                                <div class="input-group col-xs-12">
                                    <input type="text" name="photo" class="form-control file-upload-info" disabled placeholder="Upload Image">
                                    <span class="input-group-append">
                                    <button class="file-upload-browse btn btn-primary" type="button">Upload</button>
                                    </span>
                                </div>
                                <img id="output" width="20%" style="margin-top:5px;" />

                                <div class="help-block with-errors"></div>
                                @error('image')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>    
                        


                        <div class="col-md-12">
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">{{ __('Submit')}}</button>
                            </div>
                        </div>
                    </div>

                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
</div>
<!-- push external js -->
@endsection
