@extends('layouts.main')
@section('title', 'Products Stock')
@section('content')
    
    <div class="container-fluid">
    	<div class="page-header">
            <div class="row align-items-end">
                <div class="col-lg-8">
                    <div class="page-header-title">
                        <i class="ik ik-users bg-blue"></i>
                        <div class="d-inline">
                            <h5>{{ __('Products')}}</h5>
                            <span>{{ __('Stock of Product')}}</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <nav class="breadcrumb-container" aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="{{route('dashboard')}}"><i class="ik ik-home"></i></a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="#">{{ __('Product Stock')}}</a>
                            </li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
        <div class="row">
            <!-- start message area-->
            @include('include.message')
            <!-- end message area-->
            <div class="col-md-12">
                <div class="card p-3">
                    <div class="card-header"><h3>{{ __('Product')}}</h3></div>
                    <div class="card-body">
                        <table id="data_table" class="table">
                            <thead>
                                <tr>
                                    <th width="10%">{{ __('Sl')}}</th>
                                    <th width="30%">{{ __('Book Info')}}</th>
                                    <th width="20%">{{ __('Image') }}</th>
                                    <th width="50%">{{ __('Variant + Stock') }}</th>
                                    
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($all_products as $key=>$product)
                                    <tr>
                                        <td>
                                            {{ $key+1 }}
                                        </td>
                                        <td>
                                            <p><b>{{ $product->title }}</b></p>
                                            <p>Auther : {{ $product->author->name }}</p>
                                            <p>Publisher : {{ $product->publisher->name }}</p>
                                        </td>
                                        <td>
                                            <img src="{{ asset('/backend/products/'.$product->image) }}" alt="" style="width:20%;">
                                        </td>
                                        <td>
                                            <table width="100%">
                                                <tbody>
                                                    @foreach (json_decode($product->stocks) as $key => $stock)
                                                    <tr>
                                                        <td>{{ $stock->variant }}</td>
                                                        <td>{{ $stock->price }}</td>
                                                        <td>{{ $stock->quantity }}</td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
    <!-- push external js -->
@endsection