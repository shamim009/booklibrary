@extends('layouts.main')
@section('title', 'Products')
@section('content')
    
    <div class="container-fluid">
    	<div class="page-header">
            <div class="row align-items-end">
                <div class="col-lg-8">
                    <div class="page-header-title">
                        <i class="ik ik-users bg-blue"></i>
                        <div class="d-inline">
                            <h5>{{ __('Products')}}</h5>
                            <span>{{ __('List of Product')}}</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <nav class="breadcrumb-container" aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="{{route('dashboard')}}"><i class="ik ik-home"></i></a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="#">{{ __('Product')}}</a>
                            </li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
        <div class="row">
            <!-- start message area-->
            @include('include.message')
            <!-- end message area-->
            <div class="col-md-12">
                <div class="card p-3">
                    <div class="card-header"><h3>{{ __('Product')}}</h3></div>
                    <div class="card-body">
                        <table id="data_table" class="table">
                            <thead>
                                <tr>
                                    <th width="10%">{{ __('Sl')}}</th>
                                    <th width="20%">{{ __('Name')}}</th>
                                    <th width="10%">{{ __('Author')}}</th>
                                    <th width="10%">{{ __('Publisher')}}</th>
                                    <th width="10%">{{ __('Category')}}</th>
                                    <th width="10%">{{ __('Image') }}</th>
                                    <th width="10%">{{ __('Price') }}</th>
                                    <th width="10%">{{ __('Status') }}</th>
                                    <th width="10%">{{ __('Action') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($all_products as $key=>$product)
                                    <tr>
                                        <td>
                                            {{ $key+1 }}
                                        </td>
                                        <td>{{ $product->title }}</td>
                                        <td>{{ $product->author->name }}</td>
                                        <td>{{ $product->publisher->name }}</td>
                                        <td>{{ $product->category->name }}</td>
                                        <td>
                                            <img src="{{ asset('/backend/products/'.$product->image) }}" alt="" style="width:50%;">
                                        </td>
                                        <td>
                                            Price: {{ $product->unit_price }}</br>
                                            @if($product->discount_price)
                                                  Discount Price: {{ $product->discount_price }}
                                            @endif
                                        </td>
                                        <td>
                                            @if ($product->status == 1)
                                               <span class="badge badge-success m-1">Active</span>
                                             @else
                                               <span class="badge badge-danger m-1">Inactive</span>
                                             @endif
                                        </td>
                                        <td>
                                            <div class="table-actions">
                                                <a href="{{ route('products.duplicate',$product->id) }}"><i class="ik ik-external-link text-orange"></i></a>
                                                <a href="{{ route('products.edit',$product->id) }}"><i class="ik ik-edit-2 text-green"></i></a>
                                                <a href="{{ url('product/details',$product->slug) }}"><i class="ik ik-eye text-green"></i></a>
                                                <a id="edit_button" data-id="{{ $product->id }}"><i class="ik ik-trash-2 text-red"></i></a>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                            
                        </table>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
    <!-- push external js -->
@endsection