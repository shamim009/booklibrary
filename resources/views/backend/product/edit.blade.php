@extends('layouts.main')
@section('title', 'Update Product')
@section('content')
<div class="container-fluid">
    <div class="page-header">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <i class="ik ik-user-plus bg-blue"></i>
                    <div class="d-inline">
                        <h5>{{ __('Update Product')}}</h5>
                        <span>{{ __('Update product')}}</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <nav class="breadcrumb-container" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="{{url('dashboard')}}"><i class="ik ik-home"></i></a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="#">{{ __('Update Prodduct')}}</a>
                        </li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    {{ Form::open(array('route'=>['products.updateProduct'],'method'=>'POST','files'=>true,'id'=>'choice_form')) }}
    <div class="row">
        <!-- start message area-->
        @include('include.message')
        <!-- end message area-->
        <div class="col-md-6">
            <div class="card ">
                <div class="card-header">
                    <h3>{{ __('Product Information')}}</h3>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <label for="title">{{ __('Book Title')}}<span class="text-red">*</span></label>
                        <input id="title" type="text" class="form-control @error('title') is-invalid @enderror" name="title" value="{{ old('title',$product->title) }}" placeholder="Enter Book Title" required>
                        <input type="hidden" name="product_id" value={{ $product->id }}>
                        <div class="help-block with-errors"></div>

                        @error('title')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="name">{{ __('Category')}}<span class="text-red">*</span></label>
                        <select class="form-control select2" name="category_id" required>
                            <option value="">--- Select Category ---</option>
                            @forelse ($parent_categories as $key=>$item)
                                {{ $clilds = \App\Category::where('status',true)->where('parent_id',$item->id)->get() }}
                                    <option value="{{ $item->id }}" {{ ($item->id == $product->category_id) ? 'selected':'' }}>{{ $item->name }}</option>
                                @if($clilds)
                                    @foreach ($clilds as $key=>$child)
                                        {{ $sub_childs = \App\Category::where('status',true)->where('parent_id',$child->id)->get() }}
                                        <option value="{{ $child->id }}" {{ ($child->id == $product->category_id) ? 'selected':'' }}>----- {{ $child->name }}</option>
                                        @if($sub_childs)
                                            @foreach ($sub_childs as $sub)
                                                <option value="{{ $sub->id }}" {{ ($sub->id == $product->category_id) ? 'selected':'' }}>----------- {{ $sub->name }}</option>
                                            @endforeach
                                        @endif
                                    @endforeach
                                @endif
                            @empty
                                    <option>No Data Found</option>
                            @endforelse
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="author">{{ __('Author')}}<span class="text-red">*</span></label>
                        <select class="form-control select2" name="author_id" required>
                            <option value="0">--- Select Author ---</option>
                            @foreach ($author_all as $item)
                                 <option value="{{ $item->id }}" {{ ($item->id == $product->author_id) ? 'selected':'' }}>{{ $item->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="publisher">{{ __('Publisher')}}<span class="text-red">*</span></label>
                        <select class="form-control select2" name="publisher_id" required>
                            <option value="cheese">--- Select Publiasher ---</option>
                            @foreach ($publisher_all as $item)
                                 <option value="{{ $item->id }}" {{ ($item->id == $product->publisher_id) ? 'selected':'' }}>{{ $item->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="name">{{ __('Min Quantiry')}}<span class="text-red">*</span></label>
                        <input id="min_qty" type="text" class="form-control @error('min_qty') is-invalid @enderror" name="min_qty" value="{{ old('min_qty',$product->min_qty) }}" placeholder="Enter min qty.." required>
                        <div class="help-block with-errors"></div>

                        @error('min_qty')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
            </div>

            <div class="card ">
                <div class="card-header">
                    <h3>{{ __('Other Information')}}</h3>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <label for="pages">{{ __('Book Pages')}}<span class="text-red">*</span></label>
                        <input id="pages" type="text" class="form-control @error('pages') is-invalid @enderror" name="pages" value="{{ old('pages',$product->pages) }}" placeholder="Enter page number" required>
                        <div class="help-block with-errors"></div>

                        @error('pages')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    
                    <div class="form-group">
                        <label for="dimension">{{ __('Book Dimensions')}}<span class="text-red">*</span></label>
                        <input id="dimension" type="text" class="form-control @error('dimension') is-invalid @enderror" name="dimension" value="{{ old('dimension',$product->dimension) }}" placeholder="Enter book dimensions" required>
                        <div class="help-block with-errors"></div>

                        @error('dimension')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="publication_date">{{ __('Publication Date')}}<span class="text-red">*</span></label>
                        <input id="publication_date" type="date" class="form-control @error('publication_date') is-invalid @enderror" name="publication_date" value="{{ old('publication_date',$product->publication_date) }}" required>
                        <div class="help-block with-errors"></div>

                        @error('publication_date')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="illustration_note">{{ __('Illustrations note')}}<span class="text-red">*</span></label>
                        <input id="illustration_note" type="text" class="form-control @error('illustration_note') is-invalid @enderror" name="illustration_note" value="{{ old('illustration_note',$product->illustration_note) }}" required>
                        <div class="help-block with-errors"></div>

                        @error('illustration_note')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="isbn10">{{ __('ISBN10')}}</label>
                        <input id="isbn10" type="text" class="form-control @error('isbn10') is-invalid @enderror" name="isbn10" value="{{ old('isbn10',$product->isbn10) }}">
                        <div class="help-block with-errors"></div>

                        @error('isbn10')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="isbn13">{{ __('ISBN13')}}</label>
                        <input id="isbn13" type="text" class="form-control @error('isbn13') is-invalid @enderror" name="isbn13" value="{{ old('isbn13',$product->isbn13) }}" required>
                        <div class="help-block with-errors"></div>

                        @error('isbn13')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="slug">{{ __('Slug')}}</label>
                        <input id="slug" type="text" class="form-control @error('slug') is-invalid @enderror" name="slug" value="{{ old('slug',$product->slug) }}" required>
                        <div class="help-block with-errors"></div>

                        @error('slug')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="card ">
                <div class="card-header">
                    <h3>{{ __('Book Image')}}</h3>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <label>{{ __('Old Image') }}</label><br>
                        <img src="{{ asset('/backend/products/'.$product->image) }}" alt="" style="width:20%;">
                    </div>
                    <div class="form-group">
                        <label>{{ __('Upload New Image') }}<span class="text-red">*</span></label>
                        <input type="file" name="image" class="file-upload-default" onchange="loadFile(event)">
                        <div class="input-group col-xs-12">
                            <input type="text" name="photo" class="form-control file-upload-info" disabled placeholder="Upload Image">
                            <span class="input-group-append">
                            <button class="file-upload-browse btn btn-primary" type="button">Upload</button>
                            </span>
                        </div>
                        <img id="output" width="20%" style="margin-top:5px;" />

                        <div class="help-block with-errors"></div>
                        @error('image')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
            </div>

            <div class="card ">
                <div class="card-header">
                    <h3>{{ __('Product Price + Stock')}}</h3>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <label for="name">{{ __('Unit Price')}}<span class="text-red">*</span></label>
                        <input id="unit_price" type="text" class="form-control @error('unit_price') is-invalid @enderror" name="unit_price" value="{{ old('unit_price',$product->unit_price) }}" placeholder="Enter publisher name" required>
                        <div class="help-block with-errors"></div>

                        @error('unit_price')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    
                    <div class="form-group">
                        <label for="name">{{ __('Discount Price')}}<span class="text-red">*</span></label>
                        <input id="discount_price" type="text" class="form-control @error('discount_price') is-invalid @enderror" name="discount_price" value="{{ old('discount_price',$product->discount_price) }}" placeholder="Enter publisher name">
                        <div class="help-block with-errors"></div>

                        @error('discount_price')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="name">{{ __('Discount Type')}}<span class="text-red">*</span></label>
                        <select class="form-control select2" name="discount_type">
                            <option value="1" {{ ($product->discount_type == 1)?'selected':'' }}>Flat</option>
                            <option value="2" {{ ($product->discount_type == 2)?'selected':'' }}>Percentage</option>
                        </select>
                    </div>
                </div>
            </div>
            
            <div class="card ">
                <div class="card-header">
                    <h3>{{ __('Product Varient')}}</h3>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <label for="">Book Formate</label>
                        <select class="form-control select2" id="book_format" multiple="multiple" name="book_format[]">
                            @foreach ($bokk_formats as $item)
                                <option value="{{ $item->format_name }}"
                                @foreach (json_decode($product->book_format) as $for)
                                        @if($item->format_name == $for)
                                            selected="selected"
                                        @endif
                                @endforeach
                                
                                >{{ $item->format_name }}</option>
                            @endforeach
                        </select>
                    </div>
                    
                    <div class="form-group">
                        <label for="">Language</label>
                        <select class="form-control select2" id="language" multiple="multiple" name="language[]">
                            @foreach ($book_languages as $item)
                                <option value="{{ $item->language }}" 
                                    @foreach (json_decode($product->language) as $lan)
                                         @if($item->language == $lan)
                                             selected="selected"
                                         @endif
                                    @endforeach
                                    >{{ $item->language }}</option>     
                            @endforeach
                        </select>
                    </div>

                    <div class="combination" id="combination">
                        <table  width="100%">
                            <thead>
                                <tr>
                                    <td>Variant</td>
                                    <td>Variant Price</td>
                                    <td>Quantity</td>
                                </tr>
                            </thead>
                            <tbody id="tbody">
                                @foreach (json_decode($product->stocks) as $varient)
                                    <tr>
                                        <td>
                                            <input type="text" class="form-control" name="variant[]" value="{{ $varient->variant }}" readonly>
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="price[]" value="{{ $varient->price }}">
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="quantity[]" value="{{ $varient->quantity }}">
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="card ">
                <div class="card-header">
                    <h3>{{ __('Book Details')}}</h3>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <label for="details">{{ __('Publishers Details')}}<span class="text-red">*</span></label>
                        <textarea class="summernote" name="details">{!! $product->details !!}</textarea>
                        <div class="help-block with-errors"></div>

                        @error('details')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="card ">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">{{ __('Update')}}</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
</div>

@endsection
@push('script')
    <script>
        //$('#combination').hide();
        $('#book_format').change(function() {
            make_combination();
        });
        $('#language').change(function() {
            make_combination();
        });
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        function make_combination(){
            $.ajax({
            type:"POST",
            url:'{{ route('products.combination') }}',
            data:$('#choice_form').serialize(),
            success: function(data) {
                // if(data.varient.length == 0){
                //     $('#combination').hide();
                // }else{
                //     $('#combination').show(500);
                // }
                $("#tbody").empty();
                $.map(data.varient, function(val, key) {
                    $("#tbody").append('<tr>\
                                            <td>\
                                                <input type="text" class="form-control" name="variant[]" value="'+val+'" readonly>\
                                            </td>\
                                            <td>\
                                                <input type="text" class="form-control" name="price[]" value="'+data.price+'">\
                                            </td>\
                                            <td>\
                                                <input type="text" class="form-control" name="quantity[]">\
                                            </td>\
                                        </tr>');
            });
            }
        });
    }
    </script>    
@endpush
