@extends('layouts.main')
@section('title', 'Category List')
@section('content')
    <div class="container-fluid">
    	<div class="page-header">
            <div class="row align-items-end">
                <div class="col-lg-8">
                    <div class="page-header-title">
                        <i class="ik ik-users bg-blue"></i>
                        <div class="d-inline">
                            <h5>{{ __('Category')}}</h5>
                            <span>{{ __('List of Categories')}}</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <nav class="breadcrumb-container" aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="{{route('dashboard')}}"><i class="ik ik-home"></i></a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="#">{{ __('Category')}}</a>
                            </li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
        <div class="row">
            <!-- start message area-->
            @include('include.message')
            <!-- end message area-->
            <div class="col-md-12">
                <div class="card p-3">
                    <div class="card-header"><h3>{{ __('Category')}}</h3></div>
                    <div class="card-body">
                        <table id="data_table" class="table">
                            <thead>
                                <tr>
                                    <th>{{ __('Sl')}}</th>
                                    <th>{{ __('Name')}}</th>
                                    <th>{{ __('Parent') }}</th>
                                    <th>{{ __('Priority') }}</th>
                                    <th>{{ __('Status') }}</th>
                                    <th style="text-align:right;">{{ __('Action')}}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($categories as $key => $category)
                                <tr>
                                    <td>{{ $key+1 }}</td>
                                    <td>{{ $category->name }}</td>
                                    <td>
                                        @if(!empty($category->parent))
                                           {{ $category->parent->name }}
                                        @else
                                           Root Category
                                        @endif
                                    </td>
                                    <td>{{ $category->priority }}</td>
                                    <td>
                                        @if ($category->status == 1)
                                             <a href="#"><span class=" badge badge-success">Active</span></a>
                                        @else
                                             <a href="#"><span class=" badge badge-danger">Inactive</span></a>
                                        @endif
                                    </td>
                                    <td>
                                        <div class="table-actions">
                                            <a href="#"><i class="ik ik-edit-2 text-green"></i></a>
                                            <a id="edit_button" data-id="{{ $category->id }}"><i class="ik ik-trash-2 text-red"></i></a>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @push('script')
    <script>
        $(document).on("click", "table#data_table #edit_button", function(event){
            var id = $(this).data('id').toString();
            url ="category/delete/";
            url = url.concat(id);

            swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then(function (e) {

                if (e.value === true) {
                    $.get(url, function(data){
                        Swal.fire(
                            'Deleted!',
                            'Your file has been deleted.',
                            'success'
                        );
                        location.reload();     
                    });
                } else {
                    e.dismiss;
                }

            }, function (dismiss) {
                return false;
            });
        });
    </script>
    @endpush
@endsection
