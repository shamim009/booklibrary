@extends('layouts.main')
@section('title', 'Categories')
@section('content')
    <!-- push external head elements to head -->

<div class="container-fluid">
<div class="page-header">
    <div class="row align-items-end">
        <div class="col-lg-8">
            <div class="page-header-title">
                <i class="ik ik-user-plus bg-blue"></i>
                <div class="d-inline">
                    <h5>{{ __('Add Book Category')}}</h5>
                    <span>{{ __('Create new category')}}</span>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <nav class="breadcrumb-container" aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="{{url('dashboard')}}"><i class="ik ik-home"></i></a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="#">{{ __('Add Category')}}</a>
                    </li>
                </ol>
            </nav>
        </div>
    </div>
</div>
<div class="row">
    <!-- start message area-->
    @include('include.message')
    <!-- end message area-->
    <div class="col-md-8 mx-auto">
        <div class="card ">
            <div class="card-header">
                <h3>{{ __('Add Category')}}</h3>
            </div>
            <div class="card-body">
                {!! Form::open(['route' => 'categories.store', 'method' => 'post']) !!}

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="name">{{ __('Name')}}<span class="text-red">*</span></label>
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" placeholder="Enter category name" required>
                                <div class="help-block with-errors"></div>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="name">{{ __('Parent')}}</label>
                                <select name="parent_id" id="parent" class="form-control @error('parent_id') is-invalid @enderror">
                                    <option value="0">Root Menu</option>
                                    @forelse ($parent_categories as $key=>$item)
                                        {{ $clilds = \App\Category::where('status',true)->where('parent_id',$item->id)->get() }}
                                         <option value="{{ $item->id }}">{{ $item->name }}</option>
                                        @if($clilds)
                                            @foreach ($clilds as $key=>$child)
                                               {{ $sub_childs = \App\Category::where('status',true)->where('parent_id',$child->id)->get() }}
                                                <option value="{{ $child->id }}">----- {{ $child->name }}</option>
                                                @if($sub_childs)
                                                    @foreach ($sub_childs as $sub)
                                                        <option value="{{ $sub->id }}">----------- {{ $sub->name }}</option>
                                                    @endforeach
                                                @endif
                                            @endforeach
                                        @endif
                                    @empty
                                         <option>No Data Found</option>
                                    @endforelse
                                </select>
                                <div class="help-block with-errors"></div>

                                @error('parent_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="name">{{ __('Set Priority')}}<span class="text-red">*</span></label>
                                <input id="priority" type="number" class="form-control @error('priority') is-invalid @enderror" name="priority" min="0" placeholder="Enter set priority" required>
                                <div class="help-block with-errors"></div>

                                @error('priority')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>


                        <div class="col-md-12">
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">{{ __('Submit')}}</button>
                            </div>
                        </div>
                    </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>
</div>
<!-- push external js -->
@endsection