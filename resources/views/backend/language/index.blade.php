@extends('layouts.main')
@section('title', 'Book Language')
@section('content')
    <div class="container-fluid">
    	<div class="page-header">
            <div class="row align-items-end">
                <div class="col-lg-8">
                    <div class="page-header-title">
                        <i class="ik ik-users bg-blue"></i>
                        <div class="d-inline">
                            <h5>{{ __('Book Language')}}</h5>
                            <span>{{ __('List of Language')}}</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <nav class="breadcrumb-container" aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="{{route('dashboard')}}"><i class="ik ik-home"></i></a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="#">{{ __('Book Language')}}</a>
                            </li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
        <div class="row">
            <!-- start message area-->
            @include('include.message')
            <!-- end message area-->
            <div class="col-md-12">
                <div class="card p-3">
                    <div class="card-header">
                        <div class="col-md-10">
                            <h3>{{ __('Book Language')}}</h3>
                        </div>
                        <div class="col-md-2 float-right">
                            <button class="btn btn-info" data-toggle="modal" data-target="#createModal"><i class="dripicons-plus"></i> {{__('Add Language')}} </button>
                        </div>
                    </div>
                    <div class="card-body">
                        <table id="data_table" class="table">
                            <thead>
                                <tr>
                                    <th>{{ __('Sl')}}</th>
                                    <th>{{ __('Book Language')}}</th>
                                    <th>{{ __('Status') }}</th>
                                    <th style="text-align:right;">{{ __('Action')}}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($language_all as $key => $item)
                                    <tr data-id={{ $item->id }}>
                                        <td>{{ $key+1 }}</td>
                                        <td>{{ $item->language }}</td>
                                        <td>
                                            @if ($item->status == 1)
                                                <a href="#"><span class=" badge badge-success">Active</span></a>
                                            @else
                                                <a href="#"><span class=" badge badge-danger">Inactive</span></a>
                                            @endif
                                        </td>
                                        <td>
                                            <div class="table-actions">
                                                <a href="#"  data-id="{{$item->id}}" class="open-EditbrandDialog" data-toggle="modal" data-target="#editModal"><i class="ik ik-edit-2 text-green"></i></a>
                                                <a id="edit_button" data-id="{{ $item->id }}"><i class="ik ik-trash-2 text-red"></i></a>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div id="createModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
        <div role="document" class="modal-dialog">
          <div class="modal-content">
            {!! Form::open(['route' => 'languages.store', 'method' => 'post', 'files' => true]) !!}
            <div class="modal-header">
                <h5 class="modal-title">Add Language</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
              <p class="italic"><small>{{__('The field labels marked with * are required input fields')}}.</small></p>
                <div class="form-group">
                    <label>{{__('Language')}} *</label>
                    {{Form::text('language',null,array('required' => 'required', 'class' => 'form-control', 'placeholder' => 'Type book language ...'))}}
                    @error('language')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>               
                <div class="form-group">       
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-info">Save changes</button>
                </div>
            </div>
            {{ Form::close() }}
          </div>
        </div>
    </div>

    <div id="editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
        <div role="document" class="modal-dialog">
          <div class="modal-content">
            {!! Form::open(['route' => ['languages.update',1], 'method' => 'PUT', 'files' => true]) !!}
            <div class="modal-header">
                <h5 class="modal-title">Add Language</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
              <p class="italic"><small>{{__('The field labels marked with * are required input fields')}}.</small></p>
                <div class="form-group">
                    <label>{{__('Language')}} *</label>
                    <input type="hidden" name="language_id">
                    {{Form::text('language',null,array('required' => 'required', 'class' => 'form-control', 'placeholder' => 'Type book language ...'))}}
                    @error('language')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <input type="hidden" name="language_id">               
                <div class="form-group">       
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-info">Save changes</button>
                </div>
            </div>
            {{ Form::close() }}
          </div>
        </div>
    </div>

@endsection
@push('script')
    <script src="{{ asset('vendor/sweetalert/sweetalert.all.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('.open-EditbrandDialog').on('click', function() {
                var url = "languages/"
                var id = $(this).data('id').toString();
                url = url.concat(id).concat("/edit");

                $.get(url, function(data) {
                    $("#editModal input[name='language']").val(data['language']);
                    $("#editModal input[name='language_id']").val(data['id']);
                });
            });
        });
    </script>
@endpush
