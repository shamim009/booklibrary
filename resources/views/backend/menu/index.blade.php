@extends('layouts.main')
@section('title', 'Users')
@section('content')
    <!-- push external head elements to head -->
    @push('head')
    <link rel="stylesheet" href="{{ asset('plugins/DataTables/datatables.min.css') }}">
    @endpush

    <div class="container-fluid">
    	<div class="page-header">
            <div class="row align-items-end">
                <div class="col-lg-8">
                    <div class="page-header-title">
                        <i class="ik ik-users bg-blue"></i>
                        <div class="d-inline">
                            <h5>{{ __('Menu')}}</h5>
                            <span>{{ __('List of Menu')}}</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <nav class="breadcrumb-container" aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="{{route('dashboard')}}"><i class="ik ik-home"></i></a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="#">{{ __('Menu')}}</a>
                            </li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
        <div class="row">
            <!-- start message area-->
            @include('include.message')
            <!-- end message area-->
            <div class="col-md-12">
                <div class="card p-3">
                    <div class="card-header"><h3>{{ __('Menu')}}</h3></div>
                    <div class="card-body">
                        <table id="data_table" class="table">
                            <thead>
                                <tr>
                                    <th>{{ __('Sl')}}</th>
                                    <th>{{ __('Name')}}</th>
                                    <th>{{ __('Parent') }}</th>
                                    <th>{{ __('Priority') }}</th>
                                    <th>{{ __('Status') }}</th>
                                    <th style="text-align:right;">{{ __('Action')}}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($menu_all as $key => $menu)
                                <tr>
                                    <td>{{ $key+1 }}</td>
                                    <td>{{ $menu->name }}</td>
                                    <td>
                                        @if(!empty($menu->parent))
                                           {{ $menu->parent->name }}
                                        @endif
                                    </td>
                                    <td>{{ $menu->priority }}</td>
                                    <td>
                                        @if ($menu->status == 1)
                                             <a href="#"><span class=" badge badge-success">Active</span></a>
                                        @else
                                             <a href="#"><span class=" badge badge-danger">Inactive</span></a>
                                        @endif
                                    </td>
                                    <td>
                                        <div class="table-actions">
                                            <a href="#"><i class="ik ik-edit-2 text-green"></i></a>
                                            <a href="#"><i class="ik ik-trash-2 text-red"></i></a>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
    <!-- push external js -->
    @push('script')
    <script src="{{ asset('plugins/DataTables/datatables.min.js') }}"></script>
    <script src="{{ asset('js/datatables.js') }}"></script>
    @endpush
@endsection
