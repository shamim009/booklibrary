@extends('layouts.main')
@section('title', 'Authors')
@section('content')
    <!-- push external head elements to head -->

<div class="container-fluid">
<div class="page-header">
    <div class="row align-items-end">
        <div class="col-lg-8">
            <div class="page-header-title">
                <i class="ik ik-user-plus bg-blue"></i>
                <div class="d-inline">
                    <h5>{{ __('Add Top Menu')}}</h5>
                    <span>{{ __('Create new menu')}}</span>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <nav class="breadcrumb-container" aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="{{url('dashboard')}}"><i class="ik ik-home"></i></a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="#">{{ __('Add Menu')}}</a>
                    </li>
                </ol>
            </nav>
        </div>
    </div>
</div>
<div class="row">
    <!-- start message area-->
    @include('include.message')
    <!-- end message area-->
    <div class="col-md-8 mx-auto">
        <div class="card ">
            <div class="card-header">
                <h3>{{ __('Add Menu')}}</h3>
            </div>
            <div class="card-body">
                {!! Form::open(['route' => 'menu.store', 'method' => 'post']) !!}

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="name">{{ __('Name')}}<span class="text-red">*</span></label>
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" placeholder="Enter menu name" required>
                                <div class="help-block with-errors"></div>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="name">{{ __('Parent')}}</label>
                                <select name="parent_id" id="parent" class="form-control @error('parent_id') is-invalid @enderror">
                                    <option value="0">Root Menu</option>
                                </select>
                                <div class="help-block with-errors"></div>

                                @error('parent_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="name">{{ __('Set Priority')}}<span class="text-red">*</span></label>
                                <input id="priority" type="number" class="form-control @error('priority') is-invalid @enderror" name="priority" min="0" placeholder="Enter set priority" required>
                                <div class="help-block with-errors"></div>

                                @error('priority')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>


                        <div class="col-md-12">
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">{{ __('Submit')}}</button>
                            </div>
                        </div>
                    </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>
</div>
<!-- push external js -->
@endsection
