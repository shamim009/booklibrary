<?php

use Admin\MenuController;
use Admin\AuthorController;
use Admin\PublisherController;
use Admin\CategoryController;
use Admin\ProductController;
use Admin\BookFormatController;
use Admin\TestimonialController;
use Admin\BookLanguageController;
use App\Http\Controllers\Admin\DashboardController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\Auth\ForgotPasswordController;
use App\Http\Controllers\Auth\ResetPasswordController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\RolesController;
use App\Http\Controllers\PermissionController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Route::get('/', function () { return view('home'); });
Route::get('/', [HomeController::class,'index'])->name('home');
Route::get('/publishers', [HomeController::class,'publisher'])->name('publisher');
Route::get('/publisher/details/{id}', [HomeController::class,'publishe_details'])->name('publisher.details');

Route::get('/author', [HomeController::class,'authors'])->name('author');
Route::get('/author/details/{id}', [HomeController::class,'authorDetails'])->name('authorDetails');
Route::get('/contact',[HomeController::class,'contactPage'])->name('contact');
Route::get('product/details/{id}',[HomeController::class,'product_details'])->name('product.details');
Route::post('product/details/variation_product_search',[HomeController::class,'variant_product'])->name('variant.search');





Route::get('login', [LoginController::class,'showLoginForm'])->name('login');
Route::post('login', [LoginController::class,'login']);
Route::post('register', [RegisterController::class,'register']);

Route::get('password/forget',  function () {
	return view('pages.forgot-password');
})->name('password.forget');
Route::post('password/email', [ForgotPasswordController::class,'sendResetLinkEmail'])->name('password.email');
Route::get('password/reset/{token}', [ResetPasswordController::class,'showResetForm'])->name('password.reset');
Route::post('password/reset', [ResetPasswordController::class,'reset'])->name('password.update');


Route::group(['middleware' => 'auth'], function(){
	// logout route
	Route::get('/logout', [LoginController::class,'logout']);
	Route::get('/clear-cache', [HomeController::class,'clearCache']);
	// dashboard route
    Route::get('/dashboard',[DashboardController::class,'showDashboard'])->name('dashboard');

	//only those have manage_user permission will get access
	Route::group(['middleware' => 'can:manage_user'], function(){
	    Route::get('/users', [UserController::class,'index']);
	    Route::get('/user/get-list', [UserController::class,'getUserList']);
		Route::get('/user/create', [UserController::class,'create']);
		Route::post('/user/create', [UserController::class,'store'])->name('create-user');
		Route::get('/user/{id}', [UserController::class,'edit']);
		Route::post('/user/update', [UserController::class,'update']);
		Route::get('/user/delete/{id}', [UserController::class,'delete']);
	});

	//only those have manage_role permission will get access
	Route::group(['middleware' => 'can:manage_role|manage_user'], function(){
		Route::get('/roles', [RolesController::class,'index']);
		Route::get('/role/get-list', [RolesController::class,'getRoleList']);
		Route::post('/role/create', [RolesController::class,'create']);
		Route::get('/role/edit/{id}', [RolesController::class,'edit']);
		Route::post('/role/update', [RolesController::class,'update']);
		Route::get('/role/delete/{id}', [RolesController::class,'delete']);
	});

	//only those have manage_permission permission will get access
	Route::group(['middleware' => 'can:manage_permission|manage_user'], function(){
		Route::get('/permission', [PermissionController::class,'index']);
		Route::get('/permission/get-list', [PermissionController::class,'getPermissionList']);
		Route::post('/permission/create', [PermissionController::class,'create']);
		Route::get('/permission/update', [PermissionController::class,'update']);
		Route::get('/permission/delete/{id}', [PermissionController::class,'delete']);
	});

    //only those have manage_menu permission will get access
    Route::group(['prefix' => 'dashboard','middleware' => 'can:manage_menu'], function(){
		Route::get('/menu/get-list', [UserController::class,'getMenuList']);
		Route::resource('menu',MenuController::class);
    });
    //only those have manage_author permission will get access
	Route::group(['prefix' => 'dashboard','middleware' => 'can:manage_author'], function(){
        Route::resource('authors',AuthorController::class);
		Route::get('authors/delete/{id}',[App\Http\Controllers\Admin\AuthorController::class,'delete'])->name('authors.delete');
		Route::resource('authors',AuthorController::class);
	});

	//only those have manage_publisher permission will get access
	Route::group(['prefix' => 'dashboard','middleware' => 'can:manage_publisher'], function(){
		Route::get('publisher/delete/{id}',[App\Http\Controllers\Admin\PublisherController::class,'delete'])->name('publisher.delete');
		Route::resource('publisher',PublisherController::class);
	});
   //only those have manage_category permission will get access
	Route::group(['prefix' => 'dashboard','middleware' => 'can:manage_category'], function(){
		Route::get('category/delete/{id}',[App\Http\Controllers\Admin\CategoryController::class,'delete'])->name('category.delete');
		Route::resource('categories',CategoryController::class);
	});

	//only those have manage_category permission will get access
	Route::group(['prefix' => 'dashboard','middleware' => 'can:manage_category'], function(){
		Route::post('product/combination',[App\Http\Controllers\Admin\ProductController::class,'make_combination'])->name('products.combination');
		Route::post('product/update',[App\Http\Controllers\Admin\ProductController::class,'productUpdate'])->name('products.updateProduct');
		Route::get('products/stock',[App\Http\Controllers\Admin\ProductController::class,'product_stock'])->name('products.stock');
		Route::get('products/duplicate/{id}',[App\Http\Controllers\Admin\ProductController::class,'product_duplicate'])->name('products.duplicate');
		Route::resource('products',ProductController::class);
	});

	Route::group(['prefix' => 'dashboard','middleware' => 'can:manage_testimonial'], function(){
		Route::resource('testimonials',TestimonialController::class);
	});

	Route::group(['prefix' => 'dashboard','middleware' => 'can:manage_book_format'], function(){
		Route::resource('book_formats',BookFormatController::class);
	});

	Route::group(['prefix' => 'dashboard','middleware' => 'can:manage_language'], function(){
		Route::resource('languages',BookLanguageController::class);
	});
	
	
	

	// get permissions
	Route::get('get-role-permissions-badge', [PermissionController::class,'getPermissionBadgeByRole']);
});


//Route::get('/register', function () { return view('pages.register'); });
