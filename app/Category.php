<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        "name",	"parent_id","priority",	"status"
    ];

    public function parent(){
        return $this->belongsTo('App\Category','parent_id');
    }
}
