<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        "name",	"parent_id","priority",	"status"
    ];

    public function parent(){
        return $this->belongsTo('App\Menu','parent_id');
    }

}
