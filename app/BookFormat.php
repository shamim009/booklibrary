<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BookFormat extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        "format_name",	"status"
    ];

}
