<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Publisher;
use App\Auther;
use App\BookLanguage;
use App\BookFormat;
use App\Category;
use App\Product;
use App\ProductStock;
use Image;
use Auth;
use PhpParser\Node\Stmt\TryCatch;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $all_products = Product::orderBy('id','DESC')->get();
        return view('backend.product.index',compact('all_products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $publisher_all = Publisher::where('status',true)->get();
        $author_all = Auther::where('status',true)->get();
        $bokk_formats = BookFormat::where('status',true)->get();
        $book_languages = BookLanguage::where('status',true)->get();
        $parent_categories = Category::where('parent_id',0)->where('status',true)->get();
        return view('backend.product.create',compact('publisher_all','author_all','bokk_formats','book_languages','parent_categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //return $request->all();
        $data = $request->all();
        $data['user_id'] = Auth::id();
        $data['status'] = true;
        $data['num_of_sale'] = 0;

        if($request->discount_price != null){
            if($request->discount_type == 1){
                $data['discount'] = 'Flat';
                $data['discount_price'] = $request->discount_price;
            }else{
                $data['discount'] = 'Percentage';
                $data['discount_price'] = $request->unit_price * ($request->discount_price/100);
            }
        }else{
            $data['discount'] = "";
            $data['discount_type'] = 0;
        }
        
        $data['slug'] = preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(' ', '-', $request->title)).'-'.Str::random(5);
        if($request->has('language') && count($request->language) > 0){
            $data['language'] = json_encode($request->language);
        }
        else {
            $language = array();
            $data['language'] = json_encode($language);
        }

        if($request->has('book_format') && count($request->book_format) > 0){
            $data['book_format'] = json_encode($request->book_format);
        }
        else {
            $book_format = array();
            $data['book_format'] = json_encode($book_format);
        }

        if($request->has('variant') && count($request->variant) > 0){
            $data['variant'] = json_encode($request->variant);
        }
        else {
            $variant = array();
            $data['variant'] = json_encode($variant);
        }
        

        if($request->has('image')){
            $image = $request->file('image');
            if($image){
                $data['image'] = time().'.'.$image->extension();
         
                $destinationPath = public_path('/backend/products');
                $img = Image::make($image->path());
                $success = $img->resize(800, 1130, function () {
                })->save($destinationPath.'/'.$data['image']);
            }
        }
    

        try
        {
            $product = Product::create($data);
            $variant = [];
            foreach($request->variant as $key=>$var){

                $variant['user_id'] = Auth::id();
                $variant['product_id'] = $product->id;
                $variant['variant'] = $var;

                if($request->quantity[$key] == null){
                    $variant['quantity'] = $request->min_qty;
                }else{
                    $variant['quantity'] = $request->quantity[$key];
                }

                if($request->price[$key] == null){
                    if($request->discount_price != null) {
                        if($request->discount_type == 1){
                            $dis_price = $request->discount_price;
                        }else{
                            $dis_price = $request->unit_price * ($request->discount_price/100);
                        }
                        $variant['price'] = $dis_price;
                     } else {
                        $variant['price'] = $request->unit_price;
                     } 
                }else{
                    $variant['price'] = $request->price[$key];
                }
                $variant['status'] = true;
                ProductStock::create($variant);
            }
            return redirect()->route('products.index')->with('success','Product added successfully');

        }catch (\Exception $e) {
            $bug = $e->getMessage();
            return redirect()->back()->with('error', $bug);
        }
        
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $publisher_all = Publisher::where('status',true)->get();
        $author_all = Auther::where('status',true)->get();
        $bokk_formats = BookFormat::where('status',true)->get();
        $book_languages = BookLanguage::where('status',true)->get();
        $parent_categories = Category::where('parent_id',0)->where('status',true)->get();
        $product = Product::findOrFail($id);
        return view('backend.product.edit',compact('publisher_all','author_all','bokk_formats','book_languages','parent_categories','product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function productUpdate(Request $request)
    {
        //return $request->all();
        $data = $request->all();
        $product = Product::findOrFail($request->product_id);
        if($request->discount_price != null){
            if($request->discount_type == 1){
                $data['discount'] = 'Flat';
                $data['discount_price'] = $request->discount_price;
            }else{
                $data['discount'] = 'Percentage';
                $data['discount_price'] = $request->unit_price * ($request->discount_price/100);
            }
        }else{
            $data['discount'] = "";
            $data['discount_type'] = 0;
        }

        if($request->has('language') && count($request->language) > 0){
            $data['language'] = json_encode($request->language);
        }
        else {
            $language = array();
            $data['language'] = json_encode($language);
        }

        if($request->has('book_format') && count($request->book_format) > 0){
            $data['book_format'] = json_encode($request->book_format);
        }
        else {
            $book_format = array();
            $data['book_format'] = json_encode($book_format);
        }

        if($request->has('variant') && count($request->variant) > 0){
            $data['variant'] = json_encode($request->variant);
        }
        else {
            $variant = array();
            $data['variant'] = json_encode($variant);
        }

        if($request->file('image') != null){
            $image = $request->file('image');
            if($image){
                $data['image'] = time().'.'.$image->extension();
                $destinationPath = public_path('/backend/products');
                $img = Image::make($image->path());
                $success = $img->resize(800, 1130, function () {
                })->save($destinationPath.'/'.$data['image']);

                if($success){
                    $old_image = $destinationPath.'/'.$product->image;
                    if (file_exists($old_image)) {
                        @unlink($old_image);
                    }
                }
            }

        }else{
            $input['image'] = $product->image;
        }



        try
        {
            $product->update($data);
            $variant = [];
            foreach($request->variant as $key=>$var){
                //previous variant delete
                $old_variants = ProductStock::where('product_id',$request->product_id)->get();
                foreach($old_variants as $key=>$old_variant){
                    $old_variant_data[] = $old_variant->variant;

                    if(!(in_array($old_variant->variant,$request->variant))){
                        ProductStock::where([
                           ['product_id',$request->product_id],
                           ['variant',$old_variant->variant]
                        ])->delete();
                    }
                }

                $variant['user_id'] = Auth::id();
                $variant['product_id'] = $product->id;
                $variant['variant'] = $var;

                if($request->quantity[$key] == null){
                    $variant['quantity'] = $request->min_qty;
                }else{
                    $variant['quantity'] = $request->quantity[$key];
                }

                if($request->price[$key] == null){
                    if($request->discount_price != null) {
                        if($request->discount_type == 1){
                            $dis_price = $request->discount_price;
                        }else{
                            $dis_price = $request->unit_price * ($request->discount_price/100);
                        }
                        $variant['price'] = $dis_price;
                     } else {
                        $variant['price'] = $request->unit_price;
                     } 
                }else{
                    $variant['price'] = $request->price[$key];
                }
                $variant['status'] = true;

                if(in_array($var,$old_variant_data)){
                    ProductStock::where([
                        ['product_id',$request->product_id],
                        ['variant',$var]
                     ])->update($variant);
                }else{
                    ProductStock::create($variant);
                }

            }
            return redirect()->route('products.index')->with('success','Product updated successfully');

        }catch (\Exception $e) {
            $bug = $e->getMessage();
            return redirect()->back()->with('error', $bug);
        }



    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function make_combination(Request $request){
        $varient = [];
        if(($request->has('book_format') && count($request->book_format) > 0) && ($request->has('language') && count($request->language) > 0)){
            foreach($request->language as $key=>$lang){
                foreach($request->book_format as $key=>$format){
                    $combi = $lang.'-'.$format;
                    array_push($varient, $combi);  
                }    
            }
        }else if($request->has('book_format') && count($request->book_format) > 0){
            foreach($request->book_format as $key=>$format){
                $combi = $format;
                array_push($varient, $combi);
            }
        }else if($request->has('language') && count($request->language) > 0){
            foreach($request->language as $key=>$lang){
                $combi = $lang;
                array_push($varient, $combi);
            }
        }
        if($request->unit_price > 0){
            $price = $request->unit_price;
        }else{
            $price = "";
        }
        if($request->has('discount_price') && $request->discount_price > 0){
            if($request->discount_type == 1){
                 $price = $request->discount_price;
            }else if($request->discount_type == 2){
                $price = $request->unit_price * ($request->discount_price/100);
            }
        }
        return response()->json([
            'price' => $price,
            'varient' => $varient
        ],200);
    }

    public function product_stock(){
        $all_products = Product::orderBy('id','DESC')->get();
        return view('backend.product.stock',compact('all_products'));
    }

    public function product_duplicate($id){
         $product = Product::findOrFail($id);
         $data['title'] = $product->title;
         $data['user_id'] = $product->user_id;
         $data['category_id'] = $product->category_id;
         $data['author_id'] = $product->author_id;
         $data['publisher_id'] = $product->publisher_id;
         $data['min_qty'] = $product->min_qty;
         $data['pages'] = $product->pages;
         $data['dimension'] = $product->dimension;
         $data['publication_date'] = $product->publication_date;
         $data['illustration_note'] = $product->illustration_note;
         $data['isbn10'] = $product->isbn10;
         $data['isbn13'] = $product->isbn13;
         $data['image'] = $product->image;
         $data['unit_price'] = $product->unit_price;
         $data['discount_price'] = $product->discount_price;
         $data['discount_type'] = $product->discount_type;
         $data['discount'] = $product->discount;
         $data['details'] = $product->details;
         $data['book_format'] = $product->book_format;
         $data['language'] = $product->language;
         $data['variant'] = $product->variant;
         $data['slug'] = $product->slug;
         $data['num_of_sale'] = $product->num_of_sale;
         $data['status'] = $product->status;

         try{
              $product = Product::create($data);
              
              $variants = ProductStock::where('product_id',$id)->get();
                    foreach($variants as $key=>$variant){
                        $input['user_id'] = $variant->user_id;
                        $input['product_id'] = $product->id;
                        $input['variant'] = $variant->variant;
                        $input['price'] = $variant->price;
                        $input['quantity'] = $variant->quantity;
                        $input['status'] = $variant->status;

                        ProductStock::create($input);
                    }
            return redirect()->route('products.index')->with('success','Product duplicated successfully');        
         }catch (\Exception $e) {
            $bug = $e->getMessage();
            return redirect()->back()->with('error', $bug);
        }

    }
}
