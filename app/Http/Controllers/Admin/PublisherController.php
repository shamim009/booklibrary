<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Publisher;
Use Alert;
use Image;
use Auth;

class PublisherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $all_publishers = Publisher::where('status',true)->get();
        return view('backend.publisher.index',compact('all_publishers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.publisher.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'     => 'required | string | max:100',
            'details'    => 'required | string | max:2000',
            'image' => 'required | image |mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);

        if($validator->fails()) {
            return redirect()->back()->withInput()->with('error', $validator->messages()->first());
        }

        $input = $request->all();
        $input['status'] = true;
        $input['user_id'] = Auth::id();

        $image = $request->file('image');
        if($image){
            $input['image'] = time().'.'.$image->extension();
     
            $destinationPath = public_path('/backend/publisher');
            $img = Image::make($image->path());
            $success = $img->resize(300, 300, function ($constraint) {
                $constraint->aspectRatio();
            })->save($destinationPath.'/'.$input['image']);
        }
        
        // $destinationPath = public_path('/backend/publisher/');
        // $image->move($destinationPath, $input['image']);

        try
        {
            Publisher::create($input);
            return redirect()->route('publisher.index')->with('success','Publisher added successfully');

        }catch (\Exception $e) {
            $bug = $e->getMessage();
            return redirect()->back()->with('error', $bug);

        }

    }
    

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $publisher = Publisher::find($id);
        return view('backend.publisher.edit',compact('publisher'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name'     => 'required | string | max:100',
            'details'    => 'required | string | max:2000',
            'image' => 'image |mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);

        if($validator->fails()) {
            return redirect()->back()->withInput()->with('error', $validator->messages()->first());
        }
        $input = $request->all();
        $publisher = Publisher::find($id);

        if($request->file('image') !== null){
            $image = $request->file('image');
            if($image){
                $input['image'] = time().'.'.$image->extension();
         
                $destinationPath = public_path('/backend/publisher');
                $img = Image::make($image->path());
                $success = $img->resize(300, 300, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($destinationPath.'/'.$input['image']);
                if($success){
                    $old_image = $destinationPath.'/'.$publisher->image;
                    if (file_exists($old_image)) {
                        @unlink($old_image);
                    }
                }
            }
        }else{
            $input['image'] = $publisher->image;
        }
        try
        {
            $publisher->update($input);
            return redirect()->route('publisher.index')->with('success','Publisher updated successfully');

        }catch (\Exception $e) {
            $bug = $e->getMessage();
            return redirect()->back()->with('error', $bug);

        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $publisher = Publisher::find($id);
        $destinationPath = public_path('/backend/publisher');
        $old_image = $destinationPath.'/'.$publisher->image;
            if (file_exists($old_image)) {
                @unlink($old_image);
            }
        try
        {
            $publisher->delete();
            return redirect()->route('publisher.index')->with('success','Publisher deleted successfully');

        }catch (\Exception $e) {
            $bug = $e->getMessage();
            return redirect()->back()->with('error', $bug);

        }    
    }
}
