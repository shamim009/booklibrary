<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Auther;
use Auth;
use Image;

class AuthorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $author_all = Auther::where('status',true)->get();
        return view('backend.author.index',compact('author_all'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.author.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'     => 'required | string | max:100',
            'details'    => 'required | string | max:2000',
            'image' => 'required | image |mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);

        if($validator->fails()) {
            return redirect()->back()->withInput()->with('error', $validator->messages()->first());
        }

        $input = $request->all();
        $input['status'] = true;
        $input['user_id'] = Auth::id();

        $image = $request->file('image');
        if($image){
            $input['image'] = time().'.'.$image->extension();
     
            $destinationPath = public_path('/backend/author');
            $img = Image::make($image->path());
            $success = $img->resize(300, 300, function ($constraint) {
                $constraint->aspectRatio();
            })->save($destinationPath.'/'.$input['image']);
        }

        try
        {
            Auther::create($input);
            return redirect()->route('authors.index')->with('success','Author added successfully');

        }catch (\Exception $e) {
            $bug = $e->getMessage();
            return redirect()->back()->with('error', $bug);

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $author = Auther::find($id);
        return view('backend.author.edit',compact('author'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name'     => 'required | string | max:100',
            'details'    => 'required | string | max:2000',
            'image' => 'image |mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);

        if($validator->fails()) {
            return redirect()->back()->withInput()->with('error', $validator->messages()->first());
        }

        $input = $request->all();
        $author = Auther::find($id);
        if($request->file('image') != null){
            $image = $request->file('image');
            if($image){
                $input['image'] = time().'.'.$image->extension();
        
                $destinationPath = public_path('/backend/author');
                $img = Image::make($image->path());
                $success = $img->resize(300, 300, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($destinationPath.'/'.$input['image']);
                if($success){
                    $old_image = $destinationPath.'/'.$author->image;
                    if (file_exists($old_image)) {
                        @unlink($old_image);
                    }
                }
            }

        }else{
            $input['image'] = $author->image;
        }

        try
        {
            $author->update($input);
            return redirect()->route('authors.index')->with('success','Author updated successfully');

        }catch (\Exception $e) {
            $bug = $e->getMessage();
            return redirect()->back()->with('error', $bug);

        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $author = Auther::find($id);
        $destinationPath = public_path('/backend/author');
        $old_image = $destinationPath.'/'.$author->image;
            if (file_exists($old_image)) {
                @unlink($old_image);
            }
        try
        {
            $author->delete();
            return redirect()->route('authors.index')->with('success','Author deleted successfully');

        }catch (\Exception $e) {
            $bug = $e->getMessage();
            return redirect()->back()->with('error', $bug);

        }     
    }
}
