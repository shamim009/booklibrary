<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Auther;
use App\Publisher;
use App\Product;
use App\ProductStock;

class HomeController extends Controller
{


    public function index()
    {
        $authors = Auther::where('status',true)->inRandomOrder()->limit(12)->get();
        $best_selling_products = Product::where('status',true)->orderBy('num_of_sale','ASC')->get();
        return view('frontend.home',compact('authors','best_selling_products'));
    }

    public function authors(){
        $authors = Auther::where('status',true)->inRandomOrder()->limit(12)->get();
    	return view('frontend.author',compact('authors'));
    }

    public function authorDetails($id){
        $author = Auther::findOrFail($id);
        return view('frontend.author_details',compact('author'));
    }

    public function publisher(){
        $publishers = Publisher::where('status',true)->inRandomOrder()->limit(12)->get();
    	return view('frontend.publisher',compact('publishers'));
    }

    public function publishe_details($id){
        $publisher = Publisher::findOrFail($id);
        return view('frontend.publisher_detail',compact('publisher'));
    }

    public function contactPage(){
        return view('frontend.contact');
    }

    public function product_details($id){
        $product = Product::where('slug',$id)->first();
        return view('frontend.product_details',compact('product'));
    }

    public function variant_product(Request $request){
         $product = Product::find($request->id);
         $variant = $request->language.'-'.$request->book_format;
         $product_variation = ProductStock::where('product_id',$request->id)->where('variant',$variant)->first();

         return response()->json([
             'product'=>$product,
             'variant'=>$product_variation
         ],200);
    }

    public function clearCache()
    {
        \Artisan::call('cache:clear');
        return view('clear-cache');
    }
}
