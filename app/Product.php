<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $fillable = [
        "title",	"user_id",	"category_id",	"author_id",	"publisher_id",	"min_qty",	"pages",	"dimension",	"publication_date",	"illustration_note",	"isbn10",	"isbn13",	"image",	"unit_price",	"discount_price",	"discount_type",	"discount",	"book_format",	"language",	"variant",	"slug",	"num_of_sale",	"status"
    ];

    public function user(){
        return $this->belongsTo('App\User','user_id');
    }

    public function category(){
        return $this->belongsTo('App\Category','category_id');
    }

    public function author(){
        return $this->belongsTo('App\Auther','author_id');
    }

    public function publisher(){
        return $this->belongsTo('App\Publisher','publisher_id');
    }

    public function stocks(){
        return $this->hasMany('App\ProductStock');
    }


}
